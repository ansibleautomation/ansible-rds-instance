Role Name
=========

Creates RDS instance with customized parameters.

Requirements
------------

boto

Role Variables
--------------
The default variable values of the created instance are below:

    $ cat defaults/main.yml
    ---
    # defaults file for ansible-rds-instance
    #
    aws_rds_region: "{{ aws_region | default('region: us-east-2') }}"
    aws_rds_wait: 900
    aws_rds_async_retries: 120
    
    
    engine_type: MySQL
    engine_version:
    db_instance_identifier: ansible-test-aurora-db-instance
    instance_type: db.t2.micro
    password: admintest
    username: admintest
    cluster_id:
    allocated_storage: 10
    kms_key_id:
    availability_zone:
    multi_az: no
    encryption: no
    
Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    ---
    - hosts: localhost
      vars:
        instance_type: db.t2.small
        allocated_storage: 20
      roles:
        - ansible-rds-instance

License
-------

BSD

